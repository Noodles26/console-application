﻿#include <iostream>


int main()

{
    const int n = 2;
    int array[n][n] = { {0,1}, {2,3} };
    
    for (int i = 0; i < n; i++)

    {
        for (int j = 0; j < n; j++)

        {
            array[i][j] = i + j;    
            std::cout << array[i][j];
        }
        std::cout << '\n';
    }

    int today = 9;
    const int i = today % n;
    int sum = 0;

    for (int j = 0; j < n; ++j)
    {
        sum += array[i][j];
    }
   
    
    std::cout << sum << '\n';


}
