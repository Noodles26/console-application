﻿#include <iostream>


class Animal {
public:
    virtual void voice() { std::cout << "Animal" << std::endl; }
    
};

class Cat : public Animal {
public:
    void voice() override { std::cout << "Meow!" << std::endl; }
}; 

class Dog : public Animal {
public:
    void voice() override { std::cout << "Woof!" << std::endl; }
    
};

class Mouse : public Animal {
public:
    void voice() override { std::cout << "Pepe!" << std::endl; }
    
};


int main()
{

    Animal animal;
    Cat cat;
    Dog dog;
    Mouse mouse;

    Animal *array[4] = {&animal,&cat,&dog,&mouse};

    for (int i = 0; i < 4; i++)
    {
        array[i]->voice(); 

    };

}